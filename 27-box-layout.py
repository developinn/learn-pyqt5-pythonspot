import sys
from PyQt5.QtWidgets import (QApplication, QComboBox, QDialog,
        QDialogButtonBox, QFormLayout, QGridLayout, QGroupBox, QHBoxLayout,
        QLabel, QLineEdit, QMenu, QMenuBar, QPushButton, QSpinBox, QTextEdit,
        QVBoxLayout)
 
class App(QDialog):
 
    def __init__(self):
        super(QDialog, self).__init__()
        self.setWindowTitle("Form Layout - pythonspot.com")
 
        b1 = QPushButton("Button 1")
        b2 = QPushButton("Button 2")
        b3 = QPushButton("Button 3")
        b4 = QPushButton("Button 4")
 
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(b1)
        mainLayout.addWidget(b2)
        mainLayout.addWidget(b3)
        mainLayout.addWidget(b4)
 
        self.setLayout(mainLayout)

        self.show()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())