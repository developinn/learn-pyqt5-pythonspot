import sys
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = "PyQt5 - Inout Dialog"
        self.left = 100
        self.top = 100
        self.width = 640
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        self.getInteger()
        self.getText()
        self.getDouble()
        self.getChoice()

    def getInteger(self):
        val, isOk = QInputDialog.getInt(self, "Get Integer", "Value: ", 28, 0, 100, 1)
        if isOk:
            print(val)
    
    def getText(self):
        val, isOk = QInputDialog.getText(self, "Get Text", "Value: ", QLineEdit.Normal, "")
        if isOk:
            print(val)

    def getDouble(self):
        val, isOk = QInputDialog.getDouble(self, "Get Double", "Value: ", 10.50, 0, 100, 10)
        if isOk:
            print(val)
    
    def getChoice(self):
        items = ("Blue", "Green", "Red")
        val, isOk = QInputDialog.getItem(self, "Get Item", "Value: ", items, 0, False)
        if isOk:
            print(val)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())