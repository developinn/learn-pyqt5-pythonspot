import sys
from PyQt5.QtWidgets import (QApplication, QComboBox, QDialog, QDialogButtonBox, QFormLayout, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit, QMenu, QMenuBar, QPushButton, QSpinBox, QTextEdit, QVBoxLayout)

class Dialog(QDialog):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 - Signals & Slots'
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        button = QPushButton("Signal")
        button.clicked.connect(self.slotMethod)
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(button)
        self.setLayout(mainLayout)

        self.show()

    def slotMethod(self):
        print("Slot Method called !")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = Dialog()
    sys.exit(app.exec_())