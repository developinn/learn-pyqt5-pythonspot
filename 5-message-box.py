import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = "PyQt5 - Message Box"
        self.initUI()

    def initUI(self):
        while True:
            reply = QMessageBox.question(self, 'PyQt5 - Message Box', 'Have you had breakfast?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                print("Guest has had breakfast.")
            else:
                print("Cook for guest.")

    def onClick(self):
        print('Foo button clicked !')

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())