import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QAction, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = "PyQt5 - Message Box"
        self.left = 100
        self.top = 100
        self.width = 200
        self.height = 100
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(150, 25)

        self.button = QPushButton('Display Text', self)
        self.button.move(20, 50)
        self.button.clicked.connect(self.onClick)

        self.show()

    def onClick(self):
        text = self.textbox.text()
        QMessageBox.question(self, 'Message Alert', 'You typed: ' + str(text), QMessageBox.Ok, QMessageBox.Ok)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())