import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QTabWidget, QPushButton, QVBoxLayout
from PyQt5.QtCore import pyqtSlot

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = "PyQt5 - Tabs"
        self.left = 100
        self.top = 100
        self.width = 300
        self.height = 200

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.tableWidget = MaiTableWidget(self)
        self.setCentralWidget(self.tableWidget)

        self.show()

class MaiTableWidget(QWidget):

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)

        self.tabA = QWidget()
        self.tabB = QWidget()
        self.tabs = QTabWidget()
        self.tabs.resize(300, 200)
        self.tabs.addTab(self.tabA, "A")
        self.tabs.addTab(self.tabB, "B")

        self.tabA.layout = QVBoxLayout(self)
        self.pushButton = QPushButton("A Button")
        self.tabA.layout.addWidget(self.pushButton)
        self.tabA.setLayout(self.tabA.layout)

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.createTable()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

        self.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())